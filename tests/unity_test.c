

#include "unity.h"
#include "functions.h"

void setUp(void) {
    // set stuff up here
}

void tearDown(void) {
    // clean stuff up here
}

void test_function_add(void) {
    //test stuff
	TEST_ASSERT_EQUAL(10, perform_add(5,5));
}


// not needed when using generate_test_runner.rb
int main(void) {
    UNITY_BEGIN();
    RUN_TEST(test_function_add);
    return UNITY_END();
}
