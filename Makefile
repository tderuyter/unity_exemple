ifeq ($(OS),Windows_NT)
  ifeq ($(shell uname -s),) # not in a bash-like shell
	CLEANUP = del /F /Q
	MKDIR = mkdir
  else # in a bash-like shell, like msys
	CLEANUP = rm -f
	MKDIR = mkdir -p
  endif
	TARGET_EXTENSION=.exe
else
	CLEANUP = rm -f
	MKDIR = mkdir -p
	TARGET_EXTENSION=out
endif

.PHONY: clean
.PHONY: all

PATHU = Unity/src/
PATHS = src/
PATHT = tests/
PATHB = build/
PATHO = build/objs/

BUILD_PATHS = $(PATHB) $(PATHO) 
PASSED = `grep -s PASS $(PATHR)*.txt`
FAIL = `grep -s FAIL $(PATHR)*.txt`
IGNORE = `grep -s IGNORE $(PATHR)*.txt`

OBJS =  $(patsubst $(PATHT)%.c,$(PATHO)%.o,$(wildcard $(PATHT)*.c))
OBJS += $(patsubst $(PATHS)%.c,$(PATHO)%.o,$(wildcard $(PATHS)*.c)) 
OBJS += $(patsubst $(PATHU)%.c,$(PATHO)%.o,$(wildcard $(PATHU)*.c)) 

COMPILE=gcc -c
LINK=gcc
CFLAGS=-I. -I$(PATHU) -I$(PATHS) -DTEST

TARGET=$(PATHR)unit_test.txt

all: $(PATHB) $(PATHO) $(TARGET) 
	@echo "-----------------------\nIGNORES:\n-----------------------"
	@echo "$(IGNORE)"
	@echo "-----------------------\nFAILURES:\n-----------------------"
	@echo "$(FAIL)"
	@echo "-----------------------\nPASSED:\n-----------------------"
	@echo "$(PASSED)"
	@echo "\nDONE"


$(PATHB)unit_test.$(TARGET_EXTENSION): $(OBJS)
	$(LINK) -o $@ $^

$(PATHR)%.txt: $(PATHB)%.$(TARGET_EXTENSION)
	-./$< > $@ 2>&1

$(PATHB)%.$(TARGET_EXTENSION): $(PATHO)%.o $(PATHO)%.o $(PATHU)unity.o 
	$(LINK) -o $@ $^

$(PATHO)%.o:: $(PATHT)%.c
	$(COMPILE) $(CFLAGS) $< -o $@

$(PATHO)%.o:: $(PATHS)%.c
	$(COMPILE) $(CFLAGS) $< -o $@

$(PATHO)%.o:: $(PATHU)%.c $(PATHU)%.h
	$(COMPILE) $(CFLAGS) $< -o $@


$(PATHB):
	$(MKDIR) $(PATHB)

$(PATHO):
	$(MKDIR) $(PATHO)

clean:
	$(CLEANUP) $(PATHO)*.o
	$(CLEANUP) $(PATHB)*.$(TARGET_EXTENSION)

