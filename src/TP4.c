/**
 * @file
 * @author Thomas Deruuyter
 * @mail	tderuyter@groupe-atlantic.com
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "functions.h"

#ifndef TEST

int main(void) {
	int res, a, b;
	printf("!!!Hello World!!!\n");
	printf("Provide value\n");
	scanf("%d",&a);
	printf("Provide value\n");
	scanf("%d",&b);
	res = perform_add(a,b);
	printf("Result is : %d\n",res);
	return 0;
}

#endif
