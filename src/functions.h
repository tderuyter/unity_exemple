
/**
 * @file
 * @author Thomas Deruuyter
 * @mail	tderuyter@groupe-atlantic.com
 *
 * Declare utility functions
 */


/**
 * Perform an addition
 *
 * @param a [in] First member of addition
 * @param b [in] Second member of addition
 * @return sum of a and b
 */
int perform_add(int a, int b);
